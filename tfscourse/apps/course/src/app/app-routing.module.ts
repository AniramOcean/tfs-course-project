import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { BoardsComponent } from './components/boards/boards.component';
import { LoginComponent } from './components/auth/login/login.component';
import { IndexComponent } from './main/index/index.component';
import { BoardResolver } from './components/boards/board/board.resolver';
import { TasksComponent } from './components/tasks/tasks.component';
import { AuthComponent } from './components/auth/auth.component';

const routes: Route[] = [
  { path: 'login', component: AuthComponent},
  { path: '', component: MainComponent, children: [
      {path: '', component: IndexComponent},
      {
        path: ':id',
        component: BoardsComponent,
        resolve: {data: BoardResolver},
        runGuardsAndResolvers: 'paramsOrQueryParamsChange',
      },
      {path: 'boards/:id', component: TasksComponent}
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
