import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'tfscourse-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form: FormGroup;
  hidePass = true;
  hideConfPass = true;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.initRegForm();
  }

  initRegForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(6)
      ]],
      confirmPassword: ['', [
        Validators.required,
      ]]
    }, {validator: this.passwordMatchValidator});
  }

  passwordMatchValidator(form: FormGroup) {
    if (form.get('password').value !== form.get('confirmPassword').value) {
      form.controls.confirmPassword.setErrors({ mismatch: true });
    } else {
      form.controls.confirmPassword.setErrors(null);
    }
    return null;
  }

  onSubmit() {
    console.log(this.form);
    if (this.form.invalid) {
      return;
    }
  }

}
