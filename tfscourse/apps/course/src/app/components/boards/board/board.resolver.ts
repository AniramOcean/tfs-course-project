import { Board } from '../../../shared/interfaces/Board';
import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { IBoardApiService, IBoardApiServiceToken } from '../../../shared/services/backend/boards/IBoardApiService';

export interface BoardResolverData {
  userId: number;
  boards: Board[];
}

@Injectable()
export class BoardResolver implements Resolve<BoardResolverData> {
  constructor(
    @Inject(IBoardApiServiceToken)
    private boardApiService: IBoardApiService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BoardResolverData> {
    const userId = route.params.id;

    return new Observable(observer => {
      this.boardApiService.getMyBoards().subscribe(boards => {
        observer.next({
          userId,
          boards
        });
        observer.complete();
      })
    });
  }

}
