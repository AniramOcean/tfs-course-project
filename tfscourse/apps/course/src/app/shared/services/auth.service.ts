import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { backendHost } from '../const/backendHost';
import { tap } from 'rxjs/operators';

interface TokenResponse {
  access_token: string;
}

@Injectable()
export class AuthService {
  constructor(private httpClient: HttpClient) {
  }

  login(username: string, password: string): Observable<any> {
    return this.httpClient.post<TokenResponse>(`${backendHost}/auth/login`, {
      username: username,
      password: password
      }).pipe(
        tap(response => localStorage.setItem('TOKEN', response.access_token))
    );
  }
}
